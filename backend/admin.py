from django.contrib import admin
from django.core.mail import send_mail
from django.contrib.auth.models import User, Group

from backend.models import *


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'institution',
                    'section', 'subsection', 'created')
    list_filter = ('institution', 'section', 'subsection')
    search_fields = ('institution',)


@admin.register(Ebook)
class EbookAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'date',)
    list_filter = ('category',)
    exclude = ('code',)
    search_fields = ('title', 'author')


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'province', 'category',
                    'pic_email', 'pic_name', 'is_verified')
    list_filter = ('is_verified', 'category', 'province', 'city')

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if obj.is_verified:
            _ = send_mail(
                subject=f'Verifikasi Institusi Stranas KA',
                message='Verifikasi Institusi Stranas KA',
                html_message=f'<h3 style="text-align: center;"><strong>Verifikasi Institusi Stranas KA</strong></h3><p><span style="font-weight: 400;">Institusi Anda dengan nama {obj.name} telah diverifikasi oleh tim kami. Informasi institusi anda dapat dilihat secara publik pada </span><a href="https://ai-innovation.id/peta-ka"><span style="font-weight: 400;">Peta Kecerdasan Artifisial</span></a><span style="font-weight: 400;">. </span><span style="font-weight: 400;"><br/></span><span style="font-weight: 400;"><br/></span><span style="font-weight: 400;">Anda dapat mengubah informasi yang ditampilkan pada website kapan saja dengan memasukkan email penanggung jawab institusi Anda pada tempat yang disediakan.</span></p><p><span style="font-weight: 400;">Tim Stranas KA&nbsp;</span></p>',
                from_email='Tim Stranas KA',
                recipient_list=[obj.pic_email],
                fail_silently=False
            )

@admin.register(Poster)
class PosterAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'email', 'institution', 'ranking')
    list_filter = ('institution',)
    exclude = ('code',)
    search_fields = ('title', 'author', 'email', 'institution', 'ranking')

@admin.register(Materi)
class MateriAdmin(admin.ModelAdmin):
    list_display = ('id', 'keynote_session', 'day')
    list_filter = ('day',)
    exclude = ('code',)
    search_fields = ('keynote_session', 'day')

class MediaFileInline(admin.StackedInline):
    model = MediaFile
    extra = 1


class GalleryAdmin(admin.ModelAdmin):
    inlines = [MediaFileInline]
    list_per_page = 10
    search_fields = ('title',)
    exclude = ('slug',)
    list_filter = ('category',)

    def save_model(self, request, obj, form, change):
        obj.save()

        for afile in request.FILES.getlist('photos_multiple'):
            obj.photos.create(image=afile)

admin.site.register(Gallery, GalleryAdmin)

admin.site.site_header = 'Kecerdasan Artifisial Indonesia'
admin.site.unregister(User)
admin.site.unregister(Group)
