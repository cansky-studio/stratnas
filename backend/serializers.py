from rest_framework import serializers
from backend.models import *


class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institution
        fields = ['updated', 'created', 'pic_name', 'pic_phone', 'pic_email',
                  'name', 'city', 'province', 'category', 'address', 'field', 'technology', 'description', 'logo']


class EbookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ebook
        fields = ['title', 'author', 'date',
                  'files', 'url', 'category', 'code']


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['name', 'email', 'language',
                  'country', 'institution', 'section', 'subsection', 'content']


class CommentResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['updated', 'created', 'name', 'language',
                  'institution', 'section', 'subsection', 'content']


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = InlineContent
        fields = ['text', 'image']


class PosterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poster
        fields = ['title', 'author', 'email',
                  'institution', 'files', 'thumbnail', 'ranking', 'code']

class MateriSerializer(serializers.ModelSerializer):
    class Meta:
        model = Materi
        fields = ['id', 'keynote_session', 'day', 'files', 'code']

class ScaledCoordinateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScaledCoordinate
        fields = ['x1', 'y1', 'x2', 'y2', 'width', 'height']


class RectScaledCoordinateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RectScaledCoordinate
        fields = ['x1', 'y1', 'x2', 'y2', 'width', 'height']


class ScaledPositionSerializer(serializers.ModelSerializer):
    bounding_rect = ScaledCoordinateSerializer()
    rects = RectScaledCoordinateSerializer(many=True)

    class Meta:
        model = ScaledPostion
        fields = ['bounding_rect', 'rects',
                  'page_number', 'use_pdf_coordinates']


class CommentInlineSerializer(serializers.ModelSerializer):
    position = ScaledPositionSerializer()
    content = ContentSerializer()

    class Meta:
        model = CommentInline
        fields = ['updated', 'created', 'name', 'email', 'position', 'content']

    def create(self, validated_data):
        # create scaled position
        position_data = validated_data.pop('position')

        rects = position_data.pop('rects')

        bounding_rect = position_data.pop('bounding_rect')
        scaled_coordinate = ScaledCoordinate.objects.create(**bounding_rect)

        scaled_position = ScaledPostion.objects.create(
            bounding_rect=scaled_coordinate, **position_data)

        for rect in rects:
            RectScaledCoordinate.objects.create(
                scaled_position=scaled_position, **rect)

        # create inline content
        content_data = validated_data.pop('content')
        content = InlineContent.objects.create(**content_data)

        comment = CommentInline.objects.create(
            content=content, position=scaled_position, **validated_data)

        return comment
