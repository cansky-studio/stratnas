from backend.models import *
from backend.serializers import *
from backend.utils import *

from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static

from rest_framework.parsers import JSONParser
from pathlib import Path

import csv


def ebook_download(request, code):
    if request.method == 'GET':
        try:
            ebook = Ebook.objects.get(code=code)
            filename = Path(ebook.files.url).name
            response = FileResponse(
                ebook.files, as_attachment=True, filename=filename)
            return response
        except:
            return JsonResponse({}, safe=False)


@csrf_exempt
def ebook(request):
    if request.method == 'GET':
        if request.GET:
            sort = request.GET.get('sort', None)
            books = Ebook.objects.all()
            if sort:
                books = books.order_by('-date')
            serializer = EbookSerializer(books, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            books = Ebook.objects.all()
            serializer = EbookSerializer(books, many=True)
            return JsonResponse(serializer.data, safe=False)
    else:
        return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)


@csrf_exempt
def gallery_slug(request, slug):
    gallery = Gallery.objects.filter(slug=slug)
    data = gallery_serializer(gallery, many=False)
    return JsonResponse(data, safe=False)


@csrf_exempt
def gallery(request):
    if request.method == 'GET':
        if request.GET:
            category = request.GET.get('category', None)
            gallery = Gallery.objects.all()
            if category:
                gallery = gallery.filter(category=category)
            data = gallery_serializer(gallery)
            return JsonResponse(data, safe=False)
        else:
            gallery = Gallery.objects.all()
            data = gallery_serializer(gallery)
            return JsonResponse(data, safe=False)
    else:
        return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)


@csrf_exempt
def institution(request):
    if request.method == 'GET':
        if request.GET:
            name = request.GET.get('name', None)
            print(name)
            city = request.GET.get('city', None)
            province = request.GET.get('province', None)
            category = request.GET.get('category', None)
            institutions = Institution.objects.filter(is_verified=True)
            if name:
                institutions = institutions.filter(name__icontains=name)
            if city:
                institutions = institutions.filter(city__icontains=city)
            if province:
                institutions = institutions.filter(
                    province__icontains=province)
            if category:
                institutions = institutions.filter(
                    category__icontains=category)
        else:
            institutions = Institution.objects.filter(is_verified=True)

        serializer = InstitutionSerializer(institutions, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        if not request.FILES:

            data = JSONParser().parse(request)

            try:
                pic_email = data['pic_email']
                pic_name = data['pic_name']
                pic_phone = data['pic_phone']

                unique_id = generate_unique_id()

                city = data['city']
                province = data['province']
                name = data['name']
                category = data['category']
                address = data['address']
                field = data['field']
                technology = data['technology']
                description = data['description']

                validate_email(pic_email)

                institution = Institution(pic_email=pic_email, pic_name=pic_name,
                                          pic_phone=pic_phone, unique_id=unique_id,
                                          name=name, city=city, province=province,
                                          category=category, address=address,
                                          description=description, field=field,
                                          technology=technology
                                          )

                institution.save()
            except ValidationError:
                return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)
            except Exception:
                return JsonResponse({'status': 'error', 'msg': 'duplicate email'}, status=200)

            _ = send_mail(
                subject=f'Bukti Pendaftaran Institusi Stranas KA',
                message='Bukti Pendaftaran Institusi Stranas KA',
                html_message=f'<h3 style="text-align: center;">Bukti Pendaftaran Institusi Stranas KA</h3><p>Terimakasih telah mendaftarkan {institution.name} sebagai bagian dari Stranas KA.</p><p>Berikut adalah data detail institusi anda :</p><table style="height: 97px; width: 584px;"><tbody><tr><td style="width: 156px;">Nama</td><td style="width: 412px;">{institution.name}</td></tr><tr><td style="width: 156px;">Kota</td><td style="width: 412px;">{institution.city}</td></tr><tr><td style="width: 156px;">Provinsi</td><td style="width: 412px;">{institution.province}</td></tr><tr><td style="width: 156px;">Jenis Institusi</td><td style="width: 412px;">{institution.category}</td></tr><tr><td style="width: 156px;">Alamat Lengkap</td><td style="width: 412px;">{institution.address}</td></tr><tr><td style="width: 156px;">Bidang Penerapan</td><td style="width: 412px;">{field_parser(institution.field,"|")}</td></tr><tr><td style="width: 156px;">Teknologi KA</td><td style="width: 412px;">{field_parser(institution.technology,"|")}</td></tr><tr><td style="width: 156px;">Deskripsi Singkat</td><td style="width: 412px;">{institution.description}</td></tr></tbody></table><p>Dengan penanggung jawab:</p><table style="width: 467px;"><tbody><tr><td style="width: 97px;">Nama PJ</td><td style="width: 354px;">{institution.pic_name}</td></tr><tr><td style="width: 97px;">Email</td><td style="width: 354px;">{institution.pic_email}</td></tr><tr><td style="width: 97px;">Handphone</td><td style="width: 354px;">{institution.pic_phone}</td></tr></tbody></table><p><span style="font-weight: 400;">Silahkan menunggu selama tim kami melakukan verifikasi data institusi Anda. Kami akan mengirimkan email konfirmasi jika data sudah selesai di verifikasi.</span></p><p>Tim Stranas KA</p>',
                from_email='Tim Stranas KA',
                recipient_list=[pic_email],
                fail_silently=False
            )

            return JsonResponse({'status': 'success'}, status=200)
        else:
            try:
                data = request.POST
                logo = request.FILES['logo']

                pic_email = data['pic_email']
                pic_name = data['pic_name']
                pic_phone = data['pic_phone']

                unique_id = generate_unique_id()

                city = data['city']
                province = data['province']
                name = data['name']
                category = data['category']
                address = data['address']
                field = data['field']
                technology = data['technology']
                description = data['description']

                validate_email(pic_email)

                institution = Institution(pic_email=pic_email, pic_name=pic_name,
                                          pic_phone=pic_phone, unique_id=unique_id,
                                          name=name, city=city, province=province,
                                          category=category, address=address,
                                          description=description, field=field,
                                          technology=technology, logo=logo
                                          )

                institution.save()
            except ValidationError:
                return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)
            except Exception:
                return JsonResponse({'status': 'error', 'msg': 'duplicate email'}, status=200)

            _ = send_mail(
                subject=f'Bukti Pendaftaran Institusi Stranas KA',
                message='Bukti Pendaftaran Institusi Stranas KA',
                html_message=f'<h3 style="text-align: center;">Bukti Pendaftaran Institusi Stranas KA</h3><p>Terimakasih telah mendaftarkan {institution.name} sebagai bagian dari Stranas KA.</p><p>Berikut adalah data detail institusi anda :</p><table style="height: 97px; width: 584px;"><tbody><tr><td style="width: 156px;">Nama</td><td style="width: 412px;">{institution.name}</td></tr><tr><td style="width: 156px;">Kota</td><td style="width: 412px;">{institution.city}</td></tr><tr><td style="width: 156px;">Provinsi</td><td style="width: 412px;">{institution.province}</td></tr><tr><td style="width: 156px;">Jenis Institusi</td><td style="width: 412px;">{institution.category}</td></tr><tr><td style="width: 156px;">Alamat Lengkap</td><td style="width: 412px;">{institution.address}</td></tr><tr><td style="width: 156px;">Bidang Penerapan</td><td style="width: 412px;">{field_parser(institution.field,"|")}</td></tr><tr><td style="width: 156px;">Teknologi KA</td><td style="width: 412px;">{field_parser(institution.technology,"|")}</td></tr><tr><td style="width: 156px;">Deskripsi Singkat</td><td style="width: 412px;">{institution.description}</td></tr></tbody></table><p>Dengan penanggung jawab:</p><table style="width: 467px;"><tbody><tr><td style="width: 97px;">Nama PJ</td><td style="width: 354px;">{institution.pic_name}</td></tr><tr><td style="width: 97px;">Email</td><td style="width: 354px;">{institution.pic_email}</td></tr><tr><td style="width: 97px;">Handphone</td><td style="width: 354px;">{institution.pic_phone}</td></tr></tbody></table><p><span style="font-weight: 400;">Silahkan menunggu selama tim kami melakukan verifikasi data institusi Anda. Kami akan mengirimkan email konfirmasi jika data sudah selesai di verifikasi.</span></p><p>Tim Stranas KA</p>',
                from_email='Tim Stranas KA',
                recipient_list=[pic_email],
                fail_silently=False
            )

            return JsonResponse({'status': 'success'}, status=200)


@ csrf_exempt
def export_institution(request):
    if request.method == 'GET':
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Data Institusi Stratnas KA.csv"'

        writer = csv.writer(response)
        writer.writerow(['Nama Institusi', 'Alamat Lengkap',
                         'Kota', 'Provinsi', 'Bidang Penerapan', 'Teknologi KA'])

        institutions = Institution.objects.filter()
        for i in institutions:
            writer.writerow([i.name, i.address, i.city,
                             i.province, field_parser(i.field, '|'), field_parser(i.technology, '|')])

        return response


@ csrf_exempt
def confirm_institution(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)

        if 'unique_id' in data:
            try:
                unique_id = data['unique_id']
                institution = Institution.objects.get(unique_id=unique_id)
                token = generate_token(unique_id)
                serializer = InstitutionSerializer(institution)
                return JsonResponse({'status': 'success',
                                     'token': token,
                                     'data': serializer.data}, status=200)
            except Exception as e:
                print(e)
                return JsonResponse({'status': 'error', 'msg': 'not valid'}, status=200)

        else:
            try:
                pic_email = data['pic_email']
                institution = Institution.objects.get(pic_email=pic_email)
                if institution:
                    _ = send_mail(
                        subject=f'Perubahan Data Institusi',
                        message='Perubahan Data Institusi',
                        html_message=f'<h3 style="text-align: center;">Perubahan Data Institusi</h3><p><span style="font-weight: 400;">Kode verifikasi untuk perubahan data Institusi Anda pada Stratnas KA anda adalah</span></p><h4>{institution.unique_id}</h4><p><span style="font-weight: 400;">Mohon jaga kerahasiaan kode verifikasi.</span></p><p>Tim Stranas KA</p>',
                        from_email='Tim Stranas KA',
                        recipient_list=[pic_email],
                        fail_silently=False
                    )

                    return JsonResponse({'status': 'success'}, status=200)

                return JsonResponse({'status': 'error', 'msg': 'email not found'}, status=200)
            except Exception:
                return JsonResponse({'status': 'error', 'msg': 'email not found'}, status=200)


@ csrf_exempt
def update_institution(request):
    if request.method == 'POST':
        token = request.headers.get('Authorization')

        if request.FILES:
            data = request.POST
            logo = request.FILES['logo']

            try:
                pic_email = data['pic_email']
                institution = Institution.objects.get(pic_email=pic_email)

                match = match_token(token, institution.unique_id)

                if match:
                    unique_id = generate_unique_id()

                    institution.unique_id = unique_id
                    institution.pic_name = data['pic_name']
                    institution.pic_phone = data['pic_phone']
                    institution.name = data['name']
                    institution.city = data['city']
                    institution.province = data['province']
                    institution.category = data['category']
                    institution.address = data['address']
                    institution.field = data['field']
                    institution.technology = data['technology']
                    institution.description = data['description']
                    institution.logo = logo
                    institution.save()

                    serializer = InstitutionSerializer(institution)
                    return JsonResponse({'status': 'success', 'data': serializer.data}, status=200)

            except Exception as e:
                return JsonResponse({'status': 'error', 'msg': str(e)}, status=200)

        else:
            data = request.POST
            try:
                pic_email = data['pic_email']
                institution = Institution.objects.get(pic_email=pic_email)

                match = match_token(token, institution.unique_id)

                if match:
                    unique_id = generate_unique_id()

                    institution.unique_id = unique_id
                    institution.pic_name = data['pic_name']
                    institution.pic_phone = data['pic_phone']
                    institution.name = data['name']
                    institution.city = data['city']
                    institution.province = data['province']
                    institution.category = data['category']
                    institution.address = data['address']
                    institution.field = data['field']
                    institution.technology = data['technology']
                    institution.description = data['description']

                    if len(data['logo']) == 0:
                        institution.logo = None

                    institution.save()

                    serializer = InstitutionSerializer(institution)
                    return JsonResponse({'status': 'success', 'data': serializer.data}, status=200)
                else:
                    return JsonResponse({'status': 'error', 'msg': 'invalid token'}, status=200)

            except Exception as e:
                return JsonResponse({'status': 'error', 'msg': str(e)}, status=200)


@ csrf_exempt
def comment(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        if request.GET:
            language = request.GET.get('lang', 'id')
            section = request.GET.get('section', None)
            subsection = request.GET.get('subsection', None)
            comments = Comment.objects.filter(language=language)
            if section:
                comments = comments.filter(section=section)
            if subsection:
                comments = comments.filter(subsection=subsection)
        else:
            comments = Comment.objects.all()
        serializer = CommentResponseSerializer(comments, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CommentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=200)
        return JsonResponse(serializer.errors, status=400)


@ csrf_exempt
def comment_inline(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        comments = CommentInline.objects.all()
        serializer = CommentInlineSerializer(comments, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CommentInlineSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=200)
        return JsonResponse(serializer.errors, status=400)

def poster_view(request, code):
    if request.method == 'GET':
        try:
            poster = Poster.objects.get(code=code)
            filename = Path(poster.files.url).name
            response = FileResponse(
                poster.files, as_attachment=False, filename=filename)
            return response
        except:
            return JsonResponse({}, safe=False)

def poster_download(request, code):
    if request.method == 'GET':
        try:
            poster = Poster.objects.get(code=code)
            filename = Path(poster.files.url).name
            response = FileResponse(
                poster.files, as_attachment=True, filename=filename)
            return response
        except:
            return JsonResponse({}, safe=False)

@csrf_exempt
def poster_detail(request, code):
    if request.method == 'GET':
        try:
            posters = Poster.objects.get(code=code)
            serializer = PosterSerializer(posters, many=False)
            return JsonResponse(serializer.data, safe=False)
        except:
            return JsonResponse({}, safe=False)

@csrf_exempt
def poster(request):
    if request.method == 'GET':
        if request.GET:
            sort = request.GET.get('sort', None)
            posters = Poster.objects.all()
            if sort:
                posters = posters.order_by('ranking')
            serializer = PosterSerializer(posters, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            posters = Poster.objects.all().order_by('ranking')
            serializer = PosterSerializer(posters, many=True)
            return JsonResponse(serializer.data, safe=False)
    else:
        return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)

@csrf_exempt
def materi(request):
    if request.method == 'GET':
        if request.GET:
            sort = request.GET.get('sort', None)
            materi = Materi.objects.all()
            if sort:
                materi = materi.order_by('id')
            serializer = PosterSerializer(materi, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            materi = Materi.objects.all().order_by('id')
            serializer = MateriSerializer(materi, many=True)
            return JsonResponse(serializer.data, safe=False)
    else:
        return JsonResponse({'status': 'error', 'msg': 'bad request'}, status=200)

def materi_download(request, code):
    if request.method == 'GET':
        try:
            materi = Materi.objects.get(code=code)
            filename = Path(materi.files.url).name
            response = FileResponse(
                materi.files, as_attachment=True, filename=filename)
            return response
        except:
            return JsonResponse({}, safe=False)
