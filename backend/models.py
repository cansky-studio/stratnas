from django.contrib.auth.models import User
from django.db import models

import hashlib
import shortuuid

# Create your models here.


class Ebook(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    date = models.DateField()
    files = models.FileField(upload_to='ebook/%Y/%m/')
    url = models.URLField(blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    code = models.CharField(max_length=255, blank=True, null=True)

    def save(self, *args, **kwargs):
        book = f'{self.title}-{self.author}'
        self.code = hashlib.md5(book.encode()).hexdigest()
        super(Ebook, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name_plural = "Ebook"


class Gallery(models.Model):
    filetype = [
        ('video', 'video'),
        ('photo', 'photo'),
    ]

    slug = models.SlugField()
    title = models.CharField(max_length=255)
    date = models.DateField()
    category = models.CharField(max_length=10, choices=filetype)

    def save(self, *args, **kwargs):
        unique = shortuuid.uuid()
        auto_slug = f'{self.title.lower().replace(" ", "-")[:10]}-{unique}'
        self.slug = auto_slug
        super(Gallery, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title} ({self.date})'

    class Meta:
        verbose_name_plural = "Gallery"


class MediaFile(models.Model):
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
    files = models.FileField(upload_to='gallery/%Y/%m/')
    source = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    thumbnail = models.ImageField(
        upload_to='gallery/thumbnail/%Y/%m/', blank=True, null=True)

    def __str__(self):
        return f'{self.files.url}'


class Comment(models.Model):
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    language = models.CharField(default='id', max_length=255)
    country = models.CharField(max_length=255)
    institution = models.CharField(max_length=255)
    section = models.CharField(max_length=255)
    subsection = models.CharField(max_length=255)
    content = models.TextField()

    def __str__(self):
        return f'{self.email} [{self.updated}]'


class Institution(models.Model):
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    is_verified = models.BooleanField(default=False, blank=True, null=True)

    pic_name = models.CharField(max_length=255)
    pic_email = models.EmailField(unique=True)
    unique_id = models.CharField(max_length=6, unique=True)

    pic_phone = models.CharField(max_length=16, blank=True, null=True)

    name = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    province = models.CharField(max_length=255, blank=True, null=True)
    category = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    field = models.TextField(blank=True, null=True)
    technology = models.TextField(blank=True, null=True)
    logo = models.ImageField(
        upload_to='logo/', blank=True, null=True)

    def __str__(self):
        return self.name


class Coordinate(models.Model):
    left = models.FloatField()
    top = models.FloatField()
    width = models.FloatField()
    height = models.FloatField()


class ScaledCoordinate(models.Model):
    x1 = models.FloatField()
    y1 = models.FloatField()
    x2 = models.FloatField()
    y2 = models.FloatField()
    width = models.FloatField()
    height = models.FloatField()


class ScaledPostion(models.Model):
    bounding_rect = models.ForeignKey(
        ScaledCoordinate, related_name='bounding_rect', on_delete=models.CASCADE)
    page_number = models.IntegerField()
    use_pdf_coordinates = models.BooleanField(
        default=False, blank=True, null=True)


class RectScaledCoordinate(models.Model):
    scaled_position = models.ForeignKey(
        ScaledPostion, related_name='rects', on_delete=models.CASCADE)
    x1 = models.FloatField()
    y1 = models.FloatField()
    x2 = models.FloatField()
    y2 = models.FloatField()
    width = models.FloatField()
    height = models.FloatField()


class InlineContent(models.Model):
    text = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)


class CommentInline(models.Model):
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    position = models.ForeignKey(ScaledPostion, on_delete=models.CASCADE)
    content = models.ForeignKey(InlineContent, on_delete=models.CASCADE)


class Poster(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    email = models.EmailField(blank=True, null=True)
    institution = models.CharField(max_length=255)
    files = models.FileField(upload_to='poster/')
    thumbnail = models.ImageField(upload_to='poster/thumbnail/', blank=True, null=True)
    ranking = models.IntegerField(blank=True, null=True)
    code = models.CharField(max_length=255, blank=True, null=True)

    def save(self, *args, **kwargs):
        poster = f'{self.title}-{self.author}'
        self.code = hashlib.md5(poster.encode()).hexdigest()
        super(Poster, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name_plural = "Poster"

class Materi(models.Model):
    keynote_session = models.CharField(max_length=255)
    day = models.IntegerField()
    files = models.FileField(upload_to='materi/')
    code = models.CharField(max_length=255, blank=True, null=True)

    def save(self, *args, **kwargs):
        materi = f'{self.keynote_session}-{self.day}'
        self.code = hashlib.md5(materi.encode()).hexdigest()
        super(Materi, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.keynote_session}'

    class Meta:
        verbose_name_plural = "Materi"
