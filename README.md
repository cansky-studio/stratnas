# AI Innovation Backend

### Author
Naufal Ihsan Pratama

### Tech Stack
- Django Rest Framework
- dbSqlite
- Nginx
- Gunicorn

### Installation
```bash
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 8000
```

### Restart service
```bash
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
sudo systemctl restart nginx
```

### Notes
Gunicorn :
```bash
sudo nano /etc/systemd/system/gunicorn.service
```
Nginx :
```bash
sudo nano /etc/nginx/sites-available/stratnas
```
**TODO
cron job for backup db:
```bash
cp db.sqlite3 /to/path/folder
```